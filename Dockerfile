FROM alpine
RUN apk update && apk add --no-cache python3 curl unzip && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools wheel && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache
RUN apk add --no-cache gcc libxml2-dev libxslt-dev libffi-dev musl-dev openssl-dev python3-dev
RUN pip install cryptography==3.2 scrapy
RUN rm -r /root/.cache
